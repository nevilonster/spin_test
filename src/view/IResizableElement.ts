import {WindowSizeType} from "consts/WindowConsts";

interface IResizableElement {
  resize(value: WindowSizeType): void;
}

export default IResizableElement;