import Wheel from "components/wheel/Wheel";
import EventEmitter from "events";

export interface IWheelAnimation extends EventEmitter{
  setWheel(wheel:Wheel):void;
  start():void;
  setStopItemID(id:number):void;
  stop():void;
}