import {IWheelAnimation} from "view/screens/items/animation/IWheelAnimation";
import Wheel from "components/wheel/Wheel";
import {Circ, gsap} from "gsap";
import {calculateFinalAngle, degreesToRadians} from "utils/math";
import EventEmitter from "events";
import {ANIMATION_EVENTS} from "view/screens/items/animation/AnimationEvent";
import {Linear} from "gsap/gsap-core";
import TimelineChild = gsap.core.TimelineChild;

class RotateWheelAnimation extends EventEmitter implements IWheelAnimation{
  private _wheel: Wheel;
  private _timeLine: gsap.core.Timeline;
  private _time = 1;
  private _infinityTween: TimelineChild;
  private _stopItemID:number;
  constructor() {
    super();
  }
  
  setWheel(wheel: Wheel): void {
    this._wheel = wheel;
  }
  
  private continue():void{
    this._timeLine = gsap.timeline({
      repeat: -1,
    })
    this._infinityTween = gsap.to(this._wheel, {duration: this._time/2, rotation: "+=" + degreesToRadians(360), ease: Linear.easeNone});
    this._timeLine.add(this._infinityTween);
  }
  
  public start(): void {
    this._timeLine = gsap.timeline({
      onComplete: () => {
        this.continue();
      }
    });
    
    const tween = gsap.to(this._wheel, {duration: this._time, rotation: "+=" + degreesToRadians(360), ease: Circ.easeIn});
    this._timeLine.add(tween);
  }
  
  public setStopItemID(id:number):void{
    this._stopItemID = id;
  }
  
  public stop(): void {
    this._timeLine = gsap.timeline({
      onComplete: () => {
        this.emit(ANIMATION_EVENTS.COMPLETE);
      },
    })
    
    const res = calculateFinalAngle(this._wheel.countSectors, this._wheel.getIndexByID(this._stopItemID));
    this._timeLine.remove(this._infinityTween);
    this._wheel.rotation = 0;
    const tween = gsap.to(this._wheel, {duration: this._time * 2, rotation: "+=" + res, ease: Circ.easeOut});
    this._timeLine.add(tween);
  }
}

export default RotateWheelAnimation;