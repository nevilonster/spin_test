import BaseScreen from "view/screens/BaseScreen";
import {endBonusGameAction, spinBonusGameAction} from "events/actions/Actions";
import TextButton from "components/buttons/TextButton";
import {BaseButtonStyle} from "components/buttons/ButtonStyles";
import SpinButton from "components/buttons/SpinButton";
import GlobalDispatcher from "events/GlobalDispatcher";
import GameEvent from "events/GameEvent";
import {WindowSizeType} from "consts/WindowConsts";
import {BonusItemType} from "logic/BonusGameTypes";
import Wheel from "components/wheel/Wheel";
import RotateWheelAnimation from "view/screens/items/animation/RotateWheelAnimation";
import {IWheelAnimation} from "view/screens/items/animation/IWheelAnimation";
import {ANIMATION_EVENTS} from "view/screens/items/animation/AnimationEvent";
import {sound} from "@pixi/sound";
import {SoundsAssets} from "consts/assets/SoundAssetConst";
import ServerResultEvents from "events/ServerResultEvents";
import {ResponseData} from "server/response/ResponseTypes";
import WheelPointer from "components/wheel/WheelPointer";
import UserModel from "model/UserModel";
import UserEvents from "events/UserEvents";

class BonusGameScreen extends BaseScreen {
  private _bonusData: Array<BonusItemType>;
  
  private _button: TextButton;
  private _pointer:WheelPointer;
  private _wheel: Wheel;
  private _animationWheel: IWheelAnimation;
  private _winData:ResponseData;
  
  constructor(data: Array<BonusItemType>) {
    super();
    
    this._bonusData = data;
    this.initEvents();
    this.drawWheel();
  }
  
  private drawWheel(): void {
    this._wheel = new Wheel(this._bonusData);
    this.addChild(this._wheel);
    this._pointer = new WheelPointer();
    this.addChild(this._pointer);
  }
  
  protected override draw(): void {
    super.draw();
    
    this._button = new TextButton("Spin");
    this._button.setStyles(BaseButtonStyle);
    this.addChild(this._button);
    this._button.on(SpinButton.BUTTON_CLICK_EVENT, this.onClick);
  }
  
  private initEvents(): void {
    GlobalDispatcher.addEventListener(GameEvent.START_SPIN_BONUS_GAME_EVENT, this.startSpin);
    GlobalDispatcher.addEventListener(ServerResultEvents.SERVER_RESPONSE_EVENT, this.showResult);
  }
  
  public showResult = (e: ServerResultEvents):void => {
    this._winData = e.resultData;
    this._animationWheel.setStopItemID(this._winData.winItem.id);
    this._animationWheel.stop();
  }
  
  private startSpin = () => {
    sound.play(SoundsAssets.WHEEL_CLICK);
    this.lockScreen(true);
    this._animationWheel = new RotateWheelAnimation();
    this._animationWheel.setWheel(this._wheel);
    this._animationWheel.start();
    this._animationWheel.on(ANIMATION_EVENTS.COMPLETE, this.onAnimationEnd);
  }
  
  private onAnimationEnd = () => {
    sound.play(SoundsAssets.WHEEL_LANDING);
    this._wheel.highlightItem(this._winData.winItem.id)
    
    GlobalDispatcher.addEventListener(UserEvents.ON_COMPLETE_UPDATE_BALANCE_EVENT, this.onCompleteBonusGame)
    UserModel.instance.addWinCoins(this._winData.winItem.value);
  }
  
  private onCompleteBonusGame = () => {
    GlobalDispatcher.removeEventListener(UserEvents.ON_COMPLETE_UPDATE_BALANCE_EVENT, this.onCompleteBonusGame)
  
    endBonusGameAction();
  }
  
  private lockScreen = (value:boolean) => {
    this.interactiveChildren = !value;
  }
  
  private onClick(): void {
    spinBonusGameAction();
  }
  
  resize(value: WindowSizeType) {
    super.resize(value);
    const bounds = this.getBounds();
    
    const pointerBounds = this._pointer.getBounds();
    const buttonBounds = this._button.getBounds();
    
    this._button.x = (value.width - buttonBounds.width) / 2;
    this._button.y = value.height - buttonBounds.height;
    
    this._wheel.scale.set(1);
    const wheelBounds = this._wheel.getBounds();
  
    const scalex = value.width / wheelBounds.width * 0.7;
    const scaley = value.height / wheelBounds.height * 0.7;
    
    const scale = Math.min(scalex, scaley);
    this._wheel.scale.set(scale)
    
    this._wheel.x = value.width / 2;
    this._wheel.y = value.height / 2;
    
    this._pointer.x = (value.width - pointerBounds.width) / 2;
    this._pointer.y = this._wheel.y - (this._wheel.getBounds().height + pointerBounds.height) / 2;

    const containerBounds = this.getBounds();
    
  }
  
  private removeEvents(): void {
    GlobalDispatcher.removeEventListener(GameEvent.START_SPIN_BONUS_GAME_EVENT, this.startSpin);
    GlobalDispatcher.removeEventListener(ServerResultEvents.SERVER_RESPONSE_EVENT, this.showResult);
    
    this._button.off(SpinButton.BUTTON_CLICK_EVENT, this.onClick);
    this._animationWheel.off(ANIMATION_EVENTS.COMPLETE, this.onAnimationEnd);
  }
  
  public override destroy() {
    this.removeEvents();
    this.removeChild(this._button);
    this.removeChild(this._wheel);
    this._button.destroy();
    this._wheel.destroy();
  }
}

export default BonusGameScreen;
