import IResizableElement from "view/IResizableElement";
import {Sprite} from "pixi.js";

interface IScreen extends IResizableElement, Sprite{
  destroy():void;
}

export default IScreen;
