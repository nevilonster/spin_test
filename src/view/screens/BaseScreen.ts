import { Sprite } from "pixi.js";
import IScreen from "view/screens/IScreen";
import { WindowSizeType } from "consts/WindowConsts";
import {IDestroyOptions} from "@pixi/display";

class BaseScreen extends Sprite implements IScreen {
  constructor() {
    super();
    this.draw();
  }

  protected draw(): void {}

  public resize(value: WindowSizeType): void {}
  public destroy(_options?: IDestroyOptions | boolean):void{
    super.destroy(_options);
  }
}

export default BaseScreen;
