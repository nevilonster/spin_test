import {Text, TextStyle} from "pixi.js";
import { WindowSizeType } from "consts/WindowConsts";
import BaseScreen from "view/screens/BaseScreen";
import SpinButton from "components/buttons/SpinButton";
import TextButton from "components/buttons/TextButton";
import { BaseButtonStyle } from "components/buttons/ButtonStyles";
import {startBonusGameAction} from "events/actions/Actions";

class TitleScreen extends BaseScreen {
  private _button: TextButton;
  private _textComponet: Text;

  constructor() {
    super();
  }

  protected override draw(): void {
    super.draw();

    this._button = new TextButton("Start game");
    this._button.setStyles(BaseButtonStyle);
    this.addChild(this._button);
    
    this._textComponet = new Text("BONUS GAME", this.textStyle);
    this.addChild(this._textComponet);
  
    this._button.on(SpinButton.BUTTON_CLICK_EVENT, this.onClick);
  }
  
  private onClick():void{
    startBonusGameAction();
  }
  
  private get textStyle(): TextStyle {
    return new TextStyle({
      fontFamily: "Arial",
      fontSize: 44,
      fontWeight: "bold",
      fill: ["#ffffff"],
    });
  }

  resize(value: WindowSizeType) {
    super.resize(value);
    
    this._button.x = value.width / 2 - this._button.getLocalBounds().width / 2;
    this._button.y = value.height - this._button.getLocalBounds().height;
    this._textComponet.x = ( value.width - this._textComponet.getBounds().width ) / 2
    this._textComponet.y = ( value.height - this._textComponet.getBounds().height ) / 2
  }
  
  override destroy() {
    this._button.off(SpinButton.BUTTON_CLICK_EVENT, this.onClick);
    this._button.destroy();
    this.removeChild(this._button);
  }
}

export default TitleScreen;
