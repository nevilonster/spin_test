interface IAnimationScreens {
  start(): void;
  destroy(): void;
}

export default IAnimationScreens;
