import IAnimationScreens from "view/screenAnimations/IAnimationScreens";
import IScreen from "view/screens/IScreen";
import {gsap} from "gsap";
import {Container} from "pixi.js";
import TimelineChild = gsap.core.TimelineChild;
import {SCREEN_ANIMATION_TIME} from "logic/BonusGameConfig";

class ShowHideAnimationScreens implements IAnimationScreens {
  private _container: Container;
  private _screenFrom: IScreen;
  private _screenTo: IScreen;
  private _timeLine: gsap.core.Timeline;
  
  constructor(container: Container, screenFrom: IScreen, screenTo: IScreen) {
    this._container = container;
    this._screenFrom = screenFrom;
    this._screenTo = screenTo;
  }
  
  private getTween(screen: IScreen, finalAlpha: number): TimelineChild {
    return screen ? gsap.to(screen, {alpha: finalAlpha, duration: SCREEN_ANIMATION_TIME}) : null
  }
  
  public start(): void {
    this._timeLine = gsap.timeline({
      onComplete: () => {
        this.lockScreens(false);
        this.destroy();
      },
    });
    
    this.lockScreens(true);
    
    this._screenTo.alpha = 0;
    this._container.addChild(this._screenTo);
    
    const tweenFrom = this.getTween(this._screenFrom, 0);
    const tweenTo = this.getTween(this._screenTo, 1);
    
    this._timeLine.add(tweenTo, 0);
    tweenFrom && this._timeLine.add(tweenFrom, 0);
  }
  
  private lockScreens(value: boolean): void {
    this._container.interactiveChildren = !value;
  }
  
  destroy(): void {
    this._timeLine.kill();
    
    this._container.removeChild(this._screenFrom);
    this._screenFrom.destroy();
  }
}

export default ShowHideAnimationScreens;
