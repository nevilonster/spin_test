import * as PIXI from "pixi.js";

import { Container, Sprite } from "pixi.js";
import { WindowSizeType } from "consts/WindowConsts";
import IScreen from "view/screens/IScreen";
import UIView from "view/UIView";
import TitleScreen from "view/screens/TitleScreen";
import ShowHideAnimationScreens from "view/screenAnimations/ShowHideAnimationScreens";
import BonusGameScreen from "view/screens/BonusGameScreen";
import { UI_ASSETS } from "consts/assets/ImageAssetConst";
import IResizableElement from "view/IResizableElement";
import {BonusItemType} from "logic/BonusGameTypes";

class View extends Container implements IResizableElement{
  private _currentScreen: IScreen;
  private _ui: UIView;
  private _bg: Sprite;
  private _currentWindowSize: WindowSizeType;

  constructor() {
    super();
    this.init();
  }

  private init(): void {
    const texture = PIXI.Texture.from(UI_ASSETS.BACKGROUND);
    this._bg = new PIXI.Sprite(texture);
    this.addChild(this._bg);

    this._ui = new UIView();
    this._currentScreen = new TitleScreen();
    this.resizeElements();
    
    this.addChild(this._currentScreen);
    this.addChild(this._ui);
  }
  
  public gotoBonusGameScreen (values:Array<BonusItemType>):void{
    this.gotoScreen(new BonusGameScreen(values));
  }
  
  public gotoTitleScreen ():void{
    this.gotoScreen(new TitleScreen())
  }
  
  private gotoScreen(newScreen:IScreen):void{
    newScreen.resize(this._currentWindowSize);
    new ShowHideAnimationScreens(this, this._currentScreen, newScreen).start();
    this._currentScreen = newScreen;
  }
  
  private resizeElements():void{
    if(this._currentWindowSize){
      this._currentScreen.resize(this._currentWindowSize)
    }
  }

  public resize(value: WindowSizeType): void {
    this._currentWindowSize = value;
    this._ui.resize(value);
    this._bg.width = value.width;
    this._bg.height = value.height;
    this.resizeElements();
  }
}

export default View;
