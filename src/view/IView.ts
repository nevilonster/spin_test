import { WindowSizeType } from "consts/WindowConsts";

interface IView {
  resize(value: WindowSizeType): void;
}

export default IView;
