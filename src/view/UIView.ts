import { Container } from "pixi.js";
import { WindowSizeType } from "consts/WindowConsts";
import BalanceComponent from "components/balance/BalanceComponent";

class UIView extends Container {
  private _balanceComponent:BalanceComponent;
  constructor() {
    super();
    this.draw();
  }

  private draw(): void {
    this._balanceComponent = new BalanceComponent();
    this.addChild(this._balanceComponent);
  }

  public resize(value: WindowSizeType): void {
    this._balanceComponent.resize(value);
  }
}

export default UIView;
