export const degreesToRadians = (degrees: number):number =>  {
  return degrees * (Math.PI / 180);
}

export const radiansToDegrees = (radians:number): number => {
  return radians / (Math.PI / 180) ;
}

const baseAngle = 360;

export const calculateFinalAngle = (countSectors:number, index: number):number => {
  const sectorAngle = baseAngle / countSectors;
  const ang = sectorAngle * index;
  const additionalAngle = (Math.floor(Math.random() * 4) + 3) * baseAngle;
  const randomSide = Math.floor(Math.random() * 100) > 50 ? 0.5 : -0.5;
  const tempAngle = Math.floor(Math.random() * sectorAngle - 5) * randomSide;
  return degreesToRadians(-ang + additionalAngle + tempAngle);
}