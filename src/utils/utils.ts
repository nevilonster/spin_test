export async function delay(sec: number): Promise<any> {
  return await new Promise<void>((resolve) => {
    setTimeout(function () {
      resolve();
    }, sec * 1000);
  });
}

