export class BaseError extends Error {
  readonly code: number;

  constructor(code: number, message?: string) {
    super(message);
    this.code = code;
  }

  toString(): string {
    return `${this.constructor.name} ${this.code} ${this.message}`;
  }
}
