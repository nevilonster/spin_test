import {BaseEvent} from "events/GlobalDispatcher";

class UserEvents extends BaseEvent{
  public static UPDATE_USER_BALANCE_EVENT = "UPDATE_USER_BALANCE_EVENT";
  public static ANIM_UPDATE_USER_BALANCE_EVENT = "ANIM_UPDATE_USER_BALANCE_EVENT";
  public static ON_COMPLETE_UPDATE_BALANCE_EVENT = "ON_COMPLETE_UPDATE_BALANCE_EVENT";
  
  public value: number;
  
  constructor(event: string, priority = 0, value = 0) {
    super(event, priority)
    this.value = value;
  }
}

export default UserEvents;