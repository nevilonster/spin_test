import GlobalDispatcher from "../GlobalDispatcher";
import GameEvent from "events/GameEvent";
import {ResponseData} from "server/response/ResponseTypes";
import ServerResultEvents from "events/ServerResultEvents";

export type ActionType = () => void;

export const startBonusGameAction = () => {
  GlobalDispatcher.dispatchEvent(new GameEvent(GameEvent.START_BONUS_GAME_EVENT))
}

export const endBonusGameAction = () => {
  GlobalDispatcher.dispatchEvent(new GameEvent(GameEvent.END_BONUS_GAME_EVENT))
}

export const spinBonusGameAction = () => {
  GlobalDispatcher.dispatchEvent(new GameEvent(GameEvent.SPIN_BONUS_GAME_EVENT));
};

export const showResultAction = (value:ResponseData) => {
  GlobalDispatcher.dispatchEvent(new ServerResultEvents(ServerResultEvents.SERVER_RESPONSE_EVENT, 0, value));
}