import GlobalDispatcher from "events/GlobalDispatcher";
import UserEvents from "events/UserEvents";

export const updateBalanceAction = () => {
  GlobalDispatcher.dispatchEvent(new UserEvents(UserEvents.UPDATE_USER_BALANCE_EVENT))
}

export const onCompleteUpdateBalanceAction = () => {
  GlobalDispatcher.dispatchEvent(new UserEvents(UserEvents.ON_COMPLETE_UPDATE_BALANCE_EVENT));
}

export const startUpdateBalanceAction = (value:number) => {
  GlobalDispatcher.dispatchEvent(new UserEvents(UserEvents.ANIM_UPDATE_USER_BALANCE_EVENT, 0, value))
}