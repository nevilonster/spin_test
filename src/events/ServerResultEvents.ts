import {BaseEvent} from "events/GlobalDispatcher";
import {ResponseData} from "server/response/ResponseTypes";

class ServerResultEvents extends BaseEvent {
  public static SERVER_RESPONSE_EVENT = "SERVER_RESPONSE_EVENT";
  
  private _resultData: ResponseData;
  
  constructor(event: string, priority = 0, data: ResponseData) {
    super(event, priority);
    this._resultData = data;
  }
  
  public get resultData(): ResponseData {
    return this._resultData;
  }
}

export default ServerResultEvents;