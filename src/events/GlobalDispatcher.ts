type EventName = string;
type HadlerType = IEventMap[] | undefined;
type PriorityType = number;

export interface IEvent {
  name: EventName;
  priority?: number;
}

type CallbackEventType = (event: IEvent) => void;

export interface IEventMap extends IEvent {
  callback: (event: IEvent) => void;
}

export class BaseEvent implements IEvent {
  public name: EventName;
  public priority?: number = 0;

  constructor(event: string, priority = 0) {
    this.name = event;
    this.priority = priority;
  }
}

class GlobalDispatcher {
  private static _events: Map<string, IEventMap[]> = new Map<
    string,
    IEventMap[]
  >();

  public static addEventListener(
    eventName: EventName,
    callback: (event: IEvent) => void,
    priority: PriorityType = 0
  ) {
    const handlers: HadlerType = this.getHandlers(eventName);
    const handler: IEventMap = {
      name: eventName,
      priority: priority,
      callback: callback,
    };

    if (handlers === undefined) {
      this._events.set(eventName, [handler]);
    } else {
      handlers.push(handler);
    }
  }

  private static getHandlers(eventName: EventName): HadlerType {
    return this._events.get(eventName);
  }

  public static dispatchEvent(event: IEvent): void {
    const handler = this.getHandlers(event.name);
    if (handler) {
      handler.forEach((value) => {
        value.callback && value.callback(event);
      });
    }
  }

  public static removeEventListener(
    eventName: EventName,
    callback: CallbackEventType
  ) {
    const handlers: HadlerType = this.getHandlers(eventName);
    if (!handlers) {
      console.warn(`Event not founded: ${eventName}`);
      return;
    }

    handlers.forEach((value, index) => {
      if (value.callback === callback) {
        handlers.splice(index, 1);
        return;
      }
    });
  }

  public static toString(): void {
    console.info(this._events);
  }
}

export default GlobalDispatcher;
