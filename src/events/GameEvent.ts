import { BaseEvent } from "events/GlobalDispatcher";

class GameEvent extends BaseEvent {
  public static STATE_CHANGED_EVENT = "STATE_CHANGED_EVENT";
  public static GOTO_BONUS_GAME_EVENT = "GOTO_BONUS_GAME_EVENT";
  
  public static START_BONUS_GAME_EVENT = "START_BONUS_GAME";
  public static SPIN_BONUS_GAME_EVENT = "SPIN_BONUS_GAME_EVENT";
  public static START_SPIN_BONUS_GAME_EVENT = "START_SPIN_BONUS_GAME_EVENT";
  public static END_BONUS_GAME_EVENT = "END_BONUS_GAME_EVENT"
  
  constructor(
    event: string,
    priority = 0,
  ) {
    super(event, priority);
  }
}

export default GameEvent;
