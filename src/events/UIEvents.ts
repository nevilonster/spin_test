import {BaseEvent} from "events/GlobalDispatcher";

class UIEvents extends BaseEvent{
  public static ON_COMPLETE_UPDATE_BALANCE_EVENT = "ON_COMPLETE_UPDATE_BALANCE_EVENT";
  
  constructor(event: string, priority = 0) {
    super(event, priority);
  }
}

export default UIEvents;