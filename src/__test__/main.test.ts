import {degreesToRadians, radiansToDegrees} from "utils/math";

describe("Main test", () => {
  it("Converting degree to radians",  () => {
    expect(degreesToRadians(90)).toBe(1.5707963267948966);
    expect(degreesToRadians(180)).toBe(Math.PI);
    expect(degreesToRadians(360)).toBe(2*Math.PI);
  });
  it("Convert radians to degree", () => {
    expect(radiansToDegrees(0)).toBe(0);
    expect(radiansToDegrees(Math.PI)).toBe(180);
  })
});
