import {startUpdateBalanceAction, updateBalanceAction} from "events/actions/UserActions";

class UserModel {
  private static _instance:UserModel;
  private _balance: number;
  constructor(startBalance:number) {
    if(UserModel._instance){
      throw new Error("User already created use instance instead");
    }
    UserModel._instance = this;
    
    this.balance = startBalance;
  }
  
  public static get instance():UserModel {
    return UserModel._instance;
  }
  
  public spent(value:number):void{
    if(value > this._balance){
      throw new Error("insufficient funds")
    }
    
    this.balance = this._balance - value;
  }
  
  public addBalance(value:number):void{
    this.balance = this._balance + value;
  }
  
  public addWinCoins(value:number):void{
    this._balance = this._balance + value;
    startUpdateBalanceAction(value);
  }
  
  public get balance():number {
    return this._balance;
  }
  
  private set balance(value:number){
    this._balance = value;
    updateBalanceAction();
  }
}

export default UserModel;