import { Loader } from "@pixi/loaders";
import { sound } from "@pixi/sound";
import { AssetsType, IMAGES_MAP, SOUNDS_MAP } from "../consts/AssetsConst";

class LoaderManager {
  private static _instance: LoaderManager;

  private _isLoaded = false;

  constructor() {
    if (LoaderManager._instance) {
      throw new Error("");
    }
  }

  public static getInstance(): LoaderManager {
    if (!this._instance) {
      this._instance = new LoaderManager();
    }

    return this._instance;
  }

  public async loadAssets(): Promise<void> {
    return new Promise((resolve, reject) => {
      const loader = Loader.shared;
      IMAGES_MAP.forEach((value: AssetsType) => {
        loader.add(value.name, value.url);
      });
      SOUNDS_MAP.forEach((value: AssetsType) => {
        sound.add(value.name, value.url);
      });

      loader.onComplete.once(() => {
        resolve();
      });
      loader.onError.once(() => {
        reject();
      });
      loader.load();
    });
  }
}

export default LoaderManager;
