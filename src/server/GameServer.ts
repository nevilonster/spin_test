// base implementation
import { IResponse } from "./response/IResponse";
import BaseResponse from "./response/BaseResponse";
import { ResponseData } from "./response/ResponseTypes";
import { delay } from "utils/utils";
import {GameConfig} from "logic/BonusGameConfig";
import {BonusItemType} from "logic/BonusGameTypes";

type CombinationWinType = {
  weight: number;
  item: BonusItemType;
}

class GameServer {
  
  private _combinationWin:Array<CombinationWinType>;
  private _totalWeight: number;
  
  constructor() {
    this.init();
  }

  public init(): void {
    this._combinationWin = new Array<CombinationWinType>();
    this._totalWeight = 0;
    GameConfig.items.forEach((item) => {
      this._totalWeight = this._totalWeight + item.weight;
      this._combinationWin.push({weight: this._totalWeight, item})
    })
  }

  
  
  private getRandomWin():BonusItemType {
    const weight = Math.random()*this._totalWeight;
    for(let i = 0;i<this._combinationWin.length;i++){
      if(weight < this._combinationWin[i].weight){
        return this._combinationWin[i].item;
      }
    }
    return this._combinationWin[0].item;
  }

  public async spin(): Promise<IResponse> {
    await delay(3);
    const data: ResponseData = {
      timestamp: new Date().getTime(),
      winItem: this.getRandomWin()
    };

    return new BaseResponse(data);
  }
}

export default GameServer;
