import {BonusItemType} from "logic/BonusGameTypes";

export type ResponseData = {
  timestamp: number;
  winItem:BonusItemType;
};
