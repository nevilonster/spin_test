import { ResponseData } from "./ResponseTypes";

export interface IResponse {
  getData: () => ResponseData;
}
