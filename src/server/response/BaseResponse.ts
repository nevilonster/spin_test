import { IResponse } from "./IResponse";
import { ResponseData } from "./ResponseTypes";

class BaseResponse implements IResponse {
  private _data: ResponseData;
  constructor(data: ResponseData) {
    this._data = data;
  }

  public getData(): ResponseData {
    return this._data;
  }
}

export default BaseResponse;
