export type BonusItemType = {
  readonly id:number,
  value: number,
  weight: number
}

export type GameConfigType = {
  items: Array<BonusItemType>
}