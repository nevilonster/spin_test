import {GameConfigType} from "logic/BonusGameTypes";

export const GameConfig: GameConfigType = {
  items: [
    {id: 1, value: 5000, weight: 4},
    {id: 2, value: 200, weight: 100},
    {id: 3, value: 1000, weight: 20},
    {id: 4, value: 400, weight: 50},
    {id: 5, value: 2000, weight: 10},
    {id: 6, value: 200, weight: 100},
    {id: 7, value: 1000, weight: 20},
    {id: 8, value: 400, weight: 50},
  ]
}

export const SCREEN_ANIMATION_TIME = 1;