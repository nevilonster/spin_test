import {BonusItemType} from "logic/BonusGameTypes";
import {GameConfig} from "logic/BonusGameConfig";

class BonusGameLogic {
  private _items: Array<BonusItemType>;
  private _bet:number;
  constructor() {
    this._bet = 200;
    this._items = GameConfig.items;
  }
  
  public get items():Array<BonusItemType>{
    return this._items;
  }
  
  public get bet():number{
    return this._bet;
  }
}

export default BonusGameLogic;