import {Container} from "pixi.js";
import GlobalDispatcher from "events/GlobalDispatcher";
import {WindowSizeType} from "consts/WindowConsts";
import GameServer from "server/GameServer";
import {IResponse} from "server/response/IResponse";
import GameEvent from "events/GameEvent";
import View from "view/View";
import BonusGameLogic from "logic/BonusGameLogic";
import { showResultAction } from "events/actions/Actions";
import UserModel from "model/UserModel";

class Game {
  private _appContainer: Container;
  private _view: View;
  private _gameServer: GameServer;
  private _logic: BonusGameLogic;
  
  constructor(container: Container) {
    new UserModel(2000);
    
    this._appContainer = container;
    this._gameServer = new GameServer();
    this._logic = new BonusGameLogic();
    this._view = new View();
  }
  
  public resize(value: WindowSizeType): void {
    this._view.resize(value);
  }
  
  public init(): void {
    GlobalDispatcher.addEventListener(GameEvent.START_BONUS_GAME_EVENT, this.startBonusGame);
    GlobalDispatcher.addEventListener(GameEvent.END_BONUS_GAME_EVENT, this.endBonusGame);
    GlobalDispatcher.addEventListener(GameEvent.SPIN_BONUS_GAME_EVENT, this.spinBonusGame);
    
    this._appContainer.addChild(this._view);
  }
  
  private startBonusGame = (): void => {
    this._view.gotoBonusGameScreen(this._logic.items);
  }
  
  private endBonusGame = (): void => {
    this._view.gotoTitleScreen();
  }
  
  private spinBonusGame = (): void => {
    UserModel.instance.spent(this._logic.bet);
    GlobalDispatcher.dispatchEvent(new GameEvent(GameEvent.START_SPIN_BONUS_GAME_EVENT));
    this.spin();
  }
  
  private spin = async () => {
    const response: IResponse = await this._gameServer.spin();
    showResultAction(response.getData());
  };
}

export default Game;
