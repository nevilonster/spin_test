export enum UI_ASSETS {
  BACKGROUND = "bg.png",
}

export enum BUTTON_CONST {
  BTN_DISABLED = "btn_bg_disabled.png",
  BTN_HOVER = "btn_bg_hover.png",
  BTN_NORMAL = "btn_bg_normal.png",
  BTN_PRESSED = "btn_bg_pressed.png",
}
