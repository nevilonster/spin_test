export enum SoundsAssets {
  WHEEL_CLICK = "wheelClick",
  WHEEL_LANDING = "wheelLanding",
  CREDITS_ROLLUP = "creditsRollup",
}
