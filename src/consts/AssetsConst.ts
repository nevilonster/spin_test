import { UI_ASSETS } from "consts/assets/ImageAssetConst";
import { SoundsAssets } from "consts/assets/SoundAssetConst";

export type AssetsType = {
  name: string;
  url: string;
};

function getSymbolAssets(): AssetsType[] {
  return [];
}

function getUIAssets(): AssetsType[] {
  return [
    { name: UI_ASSETS.BACKGROUND, url: "images/background.png" },
    { name: "button", url: "images/button.json" },
    { name: "assets", url: "images/assets.json" },
    { name: "coin-anim", url: "images/coin-anim.json"}
  ];
}

export const IMAGES_MAP: AssetsType[] = [
  ...getSymbolAssets(),
  ...getUIAssets(),
];

function getReelSounds(): AssetsType[] {
  return [
    { name: SoundsAssets.CREDITS_ROLLUP, url: "sounds/credits-rollup.wav" },
    { name: SoundsAssets.WHEEL_LANDING, url: "sounds/wheel-landing.wav" },
    { name: SoundsAssets.WHEEL_CLICK, url: "sounds/wheel-click.wav" },
  ];
}

export const SOUNDS_MAP: AssetsType[] = [...getReelSounds()];
