import * as PIXI from "pixi.js";

import LoaderManager from "./loader/LoaderManager";
import Game from "./game/Game";

import { gsap } from "gsap";
import { PixiPlugin } from "gsap/PixiPlugin";

gsap.registerPlugin(PixiPlugin);
PixiPlugin.registerPIXI(PIXI);

const app = new PIXI.Application({
  resizeTo: window,
});

document.body.appendChild(app.view);

async function main() {
  await LoaderManager.getInstance().loadAssets();
  const game = new Game(app.stage);
  game.init();
  game.resize({ width: app.renderer.width, height: app.renderer.height });
  app.renderer.addListener("resize", onResize);
  function onResize(w: number, h: number) {
    game.resize({ width: w, height: h });
  }
}

main();
