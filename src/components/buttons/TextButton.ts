import BaseButton, { ButtonAssetType } from "components/buttons/BaseButton";
import { TextStyle, Text } from "pixi.js";

class TextButton extends BaseButton {
  private _textValue: string;
  private _textComponent: Text;

  constructor(text: string) {
    super();
    this._textValue = text;
  }

  public setStyles(assets: ButtonAssetType): void {
    super.setStyles(assets);
    this._textComponent = new Text(this._textValue, this.textStyle);
    this.addChild(this._textComponent);
    
    this.updatePositions();
  }

  private updatePositions(): void {
    this._textComponent.x = this.getBounds().width / 2 - this._textComponent.width / 2;
    this._textComponent.y = this.getBounds().height / 2 - this._textComponent.height / 2;
  }

  private get textStyle(): TextStyle {
    return new TextStyle({
      fontFamily: "Arial",
      fontSize: 24,
      fontWeight: "bold",
      fill: ["#ffffff"],
    });
  }

  public destroy() {
    this.removeChild(this._textComponent);
    super.destroy();
  }
}

export default TextButton;
