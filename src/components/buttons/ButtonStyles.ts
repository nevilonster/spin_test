import { ButtonAssetType } from "components/buttons/BaseButton";
import { BUTTON_CONST } from "consts/assets/ImageAssetConst";

export const BaseButtonStyle: ButtonAssetType = {
  normalAsset: BUTTON_CONST.BTN_NORMAL,
  pressedAsset: BUTTON_CONST.BTN_PRESSED,
  hoverAsset: BUTTON_CONST.BTN_HOVER,
  disabledAsset: BUTTON_CONST.BTN_DISABLED,
};
