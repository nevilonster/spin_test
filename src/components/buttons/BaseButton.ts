import { Sprite } from "pixi.js";
import * as PIXI from "pixi.js";
import { sound } from "@pixi/sound";

export type ButtonAssetType = {
  normalAsset: string;
  hoverAsset?: string;
  pressedAsset?: string;
  disabledAsset?: string;
  clickSound?: string;
};

class BaseButton extends Sprite {
  public static BUTTON_CLICK_EVENT = "BUTTON_CLICK_EVENT";

  private _button: Sprite;
  private _isHover = false;
  private _isPressed = false;
  private _isEnabled = true;

  private _assets: ButtonAssetType;

  constructor() {
    super();

    this.buttonMode = true;
    this.interactive = true;

    this.addEvents();
  }
  
  private addEvents():void{
    this.on("mouseover", this.mouseOver);
    this.on("mouseout", this.mouseOut);
    this.on("pointerdown", this.pressButton);
    this.on("click", this.onClick);
  }

  public setStyles(assets: ButtonAssetType): void {
    this._assets = assets;

    this._button = new PIXI.Sprite();
    this.addChild(this._button);

    this.updateTexture();
  }

  private onClick() {
    this._isPressed = false;
    if (this._isEnabled) {
      this._assets.clickSound && sound.play(this._assets.clickSound);
      this.emit(BaseButton.BUTTON_CLICK_EVENT);
    }
  }

  private mouseOver() {
    this._isHover = true;
    this.updateTexture();
  }

  private mouseOut() {
    this._isHover = false;
    this._isPressed = false;
    this.updateTexture();
  }

  private pressButton() {
    this._isPressed = true;
    this.updateTexture();
  }

  public enableButton(): void {
    this._isEnabled = true;
    this.updateTexture();
  }

  public disableButton(): void {
    this._isEnabled = false;
    this.updateTexture();
  }

  private getTextureName(): string {
    if (!this._isEnabled) {
      return this._assets.disabledAsset;
    }
    if (this._isPressed) {
      return this._assets.pressedAsset;
    }
    if (this._isHover) {
      return this._assets.hoverAsset;
    }
    return this._assets.normalAsset;
  }

  private updateTexture(): void {
    const textureName = this.getTextureName() || this._assets.normalAsset;
    const texture = PIXI.Texture.from(textureName);
    this._button.texture = texture;
  }
  
  private removeEvents():void{
    this.off("mouseover", this.mouseOver);
    this.off("mouseout", this.mouseOut);
    this.off("pointerdown", this.pressButton);
    this.off("click", this.onClick);
  }

  public destroy(): void {
    this.removeEvents();
    this.removeChild(this._button);
  }
}

export default BaseButton;
