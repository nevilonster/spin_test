import BaseButton from "components/buttons/BaseButton";
import { BUTTON_CONST } from "consts/assets/ImageAssetConst";
import { SoundsAssets } from "consts/assets/SoundAssetConst";

class SpinButton extends BaseButton {
  constructor() {
    super();
    this.setStyles({
      normalAsset: BUTTON_CONST.BTN_NORMAL,
      clickSound: SoundsAssets.WHEEL_CLICK,
    });
  }
}

export default SpinButton;
