import {AnimatedSprite, Sprite, Text, TextStyle, Texture} from "pixi.js";
import IResizableElement from "view/IResizableElement";
import {WindowSizeType} from "consts/WindowConsts";
import UserModel from "model/UserModel";
import GlobalDispatcher from "events/GlobalDispatcher";
import UserEvents from "events/UserEvents";
import {onCompleteUpdateBalanceAction} from "events/actions/UserActions";
import {Circ, gsap} from "gsap";
import {sound} from "@pixi/sound";
import {SoundsAssets} from "consts/assets/SoundAssetConst";

class BalanceComponent extends Sprite implements IResizableElement{
  private _icon:AnimatedSprite;
  private _textComponent:Text;
  private _rollupTweenLine: gsap.core.Timeline;
  
  constructor() {
    super();
    this.draw();
    this.init();
  }
  
  private draw():void{
    this.interactive = false;
    
    this._textComponent = new Text(0, this.textStyle);
    this.addChild(this._textComponent);
    
    this.createIcon();
    
    this.updateBalance();
  }
  
  private createIcon(): void {
    const textures: Texture[] = [];
    const frames = 6;
    for (let i = 1; i <= frames; i++) {
      textures.push(Texture.from(`coin-anim-0${i}.png`));
    }
    
    this._icon = new AnimatedSprite(textures);
    this._icon.scale.set(0.18, 0.18);
    this._icon.loop = false;
    this._icon.animationSpeed = 0.5;
    this.addChild(this._icon);
  }
  
  private init():void{
    this._rollupTweenLine = gsap.timeline({
      onComplete: () => {
        this._icon.loop = false;
        sound.stop(SoundsAssets.CREDITS_ROLLUP);
        
        onCompleteUpdateBalanceAction();
      },
    });
    
    GlobalDispatcher.addEventListener(UserEvents.UPDATE_USER_BALANCE_EVENT, this.updateBalance);
    GlobalDispatcher.addEventListener(UserEvents.ANIM_UPDATE_USER_BALANCE_EVENT, this.animationUpdateBalance);
  }
  
  private updateBalance = ():void => {
    this._textComponent.text = "Balance:" + UserModel.instance.balance;
    this._icon.loop = false;
    this._icon.gotoAndPlay(1);
  }
  
  private animationUpdateBalance = (e:UserEvents):void => {
    this._icon.loop = true;
    this._icon.gotoAndPlay(1);
    sound.play(SoundsAssets.CREDITS_ROLLUP, {loop: true})
  
    this._rollupTweenLine.kill();
    const tweenData = {value: UserModel.instance.balance - e.value};
    const tween = gsap.to(tweenData, 2, {
      value: UserModel.instance.balance,
      roundProps: "value",
      ease: Circ.easeOut,
      onUpdate: () => {
        this._textComponent.text = "Balance:" + (tweenData.value);
      }})
    
    this._rollupTweenLine.add(tween);
  }
  
  private get textStyle(): TextStyle {
    return new TextStyle({
      fontFamily: "Arial",
      fontSize: 24,
      fontWeight: "bold",
      fill: ["#ff0000"],
    });
  }
  
  public resize(value: WindowSizeType): void{
    this._icon.x = 0;
    this._icon.y = this._icon.getBounds().height / 2
    this._textComponent.x = this._icon.x + this._icon.getBounds().width;
    this._textComponent.y = 5;
    this.x = value.width - this.getBounds().width ;
    this.y = value.height - this.getBounds().height * 2;
  }
  
  private removeEvents():void{
    GlobalDispatcher.removeEventListener(UserEvents.UPDATE_USER_BALANCE_EVENT, this.updateBalance);
    GlobalDispatcher.removeEventListener(UserEvents.ANIM_UPDATE_USER_BALANCE_EVENT, this.animationUpdateBalance);
  }
  
  public destroy():void{
    this.removeEvents();
  }
  
}

export default BalanceComponent;