import {Sprite} from "pixi.js";

class WheelPointer extends Sprite {
  constructor() {
    super();
    this.draw();
  }
  
  private draw ():void{
    const pointer = Sprite.from('pointer.png');
    this.addChild(pointer);
  }
}

export default WheelPointer;