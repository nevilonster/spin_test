import {Sprite, Text, TextStyle} from "pixi.js";
import {BonusItemType} from "logic/BonusGameTypes";
import {WHEEL_PADDING_Y} from "consts/uiConsts";

class WheelItem extends Sprite{
  private _view:Sprite;
  
  private _textComponent: Text;
  private _data:BonusItemType;
  constructor(data:BonusItemType) {
    super();
    this._data = data;
    this.draw();
  }
  
  private draw():void{
    this._view = Sprite.from('wheel-slice.png');
    this.addChild(this._view);
    this._view.anchor.set(0.5, 1);
    
    this._textComponent = new Text(this._data.value, this.textStyle);
    this.addChild(this._textComponent);
    this._textComponent.anchor.set(0.5, 0.5);
    
    this.resize();
  }
  
  public highlight() {
    this._textComponent.style.fill = ["#ff0000"];
  }
  
  public get id():number{
    return this._data.id;
  }
  
  private resize():void{
    this._textComponent.y = WHEEL_PADDING_Y;
  }
  
  public destroy() {
    this.removeChild(this._view);
  }
  
  private get textStyle():TextStyle{
    return new TextStyle({
      fontFamily: "Arial",
      fontSize: 40,
      fontWeight: "bold",
      fill: ["#000000"]
    });
  }
}

export default WheelItem;