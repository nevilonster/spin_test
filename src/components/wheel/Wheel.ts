import {Sprite} from "pixi.js";
import WheelItem from "components/wheel/WheelItem";
import {BonusItemType} from "logic/BonusGameTypes";
import {degreesToRadians} from "utils/math";

class Wheel extends Sprite {
  private _items: Array<WheelItem> = [];
  private _data: Array<BonusItemType>;
  
  constructor(data: Array<BonusItemType>) {
    super();
    this._data = data;
    this.draw();
  }
  
  private draw(): void {
    const count = this._data.length;
    this._data.forEach((item, index) => {
      const wheelItem = new WheelItem(item);
      const angle = index * (360 / count);
      wheelItem.anchor.set(0.5, 1);
      wheelItem.rotation = degreesToRadians(angle);
      
      this._items.push(wheelItem);
      this.addChild(wheelItem);
    })
  }
  
  public get countSectors(): number {
    return this._items.length;
  }
  
  public highlightItem(id: number) {
    for(let i = 0; i < this._items.length; i++){
      if (this._items[i].id === id){
        this._items[i].highlight();
        return;
      }
    }
  }
  
  public getIndexByID(id: number): number {
    for (let i = 0; i < this._items.length; i++) {
      if (this._items[i].id === id) {
        return i;
      }
    }
    return -1
  }
  
  public destroy() {
    for (let i = 0; i < this._items.length; i++) {
      const item: WheelItem = this._items[0];
      this.removeChild(item);
      item.destroy();
    }
  }
}

export default Wheel;