import type {Config} from '@jest/types';

const config: Config.InitialOptions = {
    verbose: true,
    preset: "ts-jest",
    testEnvironment: "node",
    moduleDirectories: ['node_modules', 'src'],
    transform: {
        "^.+.ts?$":"ts-jest"
    },
};
export default config;